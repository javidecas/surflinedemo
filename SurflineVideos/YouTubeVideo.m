//
//  YouTubeVideo.m
//  SurflineVideos
//
//  Created by Javier de Castro on 12/12/15.
//  Copyright © 2015 Javidecas. All rights reserved.
//

#import "YouTubeVideo.h"

@implementation YouTubeVideo



///Create YouTubeVideo Object using Dictionary info
+ (YouTubeVideo*) YouTubeVideoWithDictionary: (NSDictionary*) dict
{
    YouTubeVideo *youTubeVideo = [[YouTubeVideo alloc] init];
    
    youTubeVideo.videoID=[[dict objectForKey:@"id"] objectForKey:@"videoId"];
    youTubeVideo.channelId = [[dict objectForKey:@"snippet"] objectForKey:@"channelId"];
    youTubeVideo.channelTitle = [[dict objectForKey:@"snippet"] objectForKey:@"channelTitle"];
    youTubeVideo.desc = [[dict objectForKey:@"snippet"] objectForKey:@"description"];
    youTubeVideo.liveBroadcastContent = [[dict objectForKey:@"snippet"] objectForKey:@"liveBroadcastContent"];
    youTubeVideo.publishedAt = [[dict objectForKey:@"snippet"] objectForKey:@"publishedAt"];
    youTubeVideo.title = [[dict objectForKey:@"snippet"] objectForKey:@"title"];
    NSDictionary * thumbNailInfo=[[dict objectForKey:@"snippet"] objectForKey:@"thumbnails"];
    youTubeVideo.defaultThumbnailURL = [[thumbNailInfo objectForKey:@"default"] objectForKey:@"url"];
    youTubeVideo.highThumbnailURL = [[thumbNailInfo objectForKey:@"high"]objectForKey:@"url"];
    youTubeVideo.mediumThumbnailURL = [[thumbNailInfo objectForKey:@"medium"]objectForKey:@"url"];
    
    return youTubeVideo;
}



//Readable Description of YouTubeVideo Object
- (NSString*) description
{
    return [NSString stringWithFormat:@"%@ | Video ID: %@ | Title: %@", [super description], self.videoID, self.title];
}


@end
