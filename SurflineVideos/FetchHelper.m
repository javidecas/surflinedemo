//
//  FetchHelper.m
//  SurflineVideos
//
//  Created by Javier de Castro on 12/12/15.
//  Copyright © 2015 Javidecas. All rights reserved.
//

#import "FetchHelper.h"
#import "YouTubeVideo.h"
@interface FetchHelper ()


@end


@implementation FetchHelper

// Use YouTube Search API to Retrieve Videos related with the searchTerm keyword and call completion block with Array of YouTubeVideo Object Results or call errorBlock
+ (void) searchVideosAbout:(NSString *) searchTerm
              onCompletion: (ArrayResponseBlock) completion
                   onError: (ErrorBlock) errorBlock
{
    
    //For simplicity, this demo App will only fetch the first 25 results
    int maxResults=25;
    //For simplicity, this demo App will only fetch Video Results ( Not Playlists or channels )
    NSString * type=@"video";
    //URL Encode the searchTerm
    NSString *properlyEscapedsearchTerm = [searchTerm stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    //Build Request URL adding relevant params:
    NSString * urlStr=[NSString stringWithFormat:@"%@?part=snippet&maxResults=%i&q=%@&type=%@&key=%@",SEARCH_API_BASE_URL_STRING,maxResults,properlyEscapedsearchTerm,type,KEY];
    //Create URL From String
    NSURL * url=[NSURL URLWithString:urlStr];
    
    //Create OUR request setting the timeout to 12 seconds
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:12.0];
    
    //Create the Task using the request and use the completionHandler Block to perform our operations once we receive the HTTP Response
    NSURLSessionDataTask *task=   [[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable connectionError) {
        //If we did receivve some data and no error
        if ([data length] >0 && connectionError == nil)
        {
            //Error to be filled by JSONObjectWithData in case of an error
            NSError *jsonError = nil;
            
            //Create JSON Dict using response Data
            NSDictionary *JSONResponseDict = [NSJSONSerialization JSONObjectWithData: data options:kNilOptions error: &jsonError];
            //If no Json Error ( Json is propperly formed ) parse the info and call completion block
            if (!jsonError)
            {
                //Init channels array
                NSMutableArray *channels =  [[NSMutableArray alloc] init];
                
                //fill array with YouTubeVideo Objects
                [self parseVideosFromDict:JSONResponseDict  onParsedResponse:channels];
                
                //Call completion block with channels array
                if (completion)
                    completion(channels);
                return;
            }else
            {
                
                NSLog(@"Error parsing Channels JSON");
                //Call Error Block with jsonError
                if(errorBlock)
                    errorBlock(jsonError);
                return;
            }
            
        }
        //If We did not receive any data and no errors
        else if ([data length] == 0 && connectionError == nil)
        {
            NSLog(@"Request Error");
            
            //Create error and Call Error Block with it
            NSError *error = [NSError errorWithDomain:@"No Data on Response" code:400 userInfo:nil];
            if(errorBlock)
                errorBlock(error);
            return;
        }
        //If receive a connection Error
        else if (connectionError != nil){
            NSLog(@"Connection Error");
            //Call Error Block with connectionError
            if(errorBlock)
                errorBlock(connectionError);
            return;
        }
    }];
    
    [task resume];
}


//Create YouTubeVideo Objects usind Dictionary and fill myParsedResponse Array with them
+ (void) parseVideosFromDict:(NSDictionary *) responseJson
            onParsedResponse:(NSMutableArray *) myParsedResponse
{
    NSMutableArray *videos=[responseJson objectForKey:@"items"];
    
    
    for (NSDictionary * videoInfo in videos) {
        [myParsedResponse addObject:[YouTubeVideo YouTubeVideoWithDictionary:videoInfo]];
    }
    
}
@end
