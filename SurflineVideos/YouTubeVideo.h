//
//  YouTubeVideo.h
//  SurflineVideos
//
//  Created by Javier de Castro on 12/12/15.
//  Copyright © 2015 Javidecas. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YouTubeVideo : NSObject

//Most Relevant info about the video to be stored in the model
@property (strong, nonatomic) NSString *videoID;
@property (strong, nonatomic) NSString *channelId;
@property (strong, nonatomic) NSString *channelTitle;
@property (strong, nonatomic) NSString *desc;
@property (strong, nonatomic) NSString *liveBroadcastContent;
@property (strong, nonatomic) NSString *publishedAt;
@property (strong, nonatomic) NSString *title;
@property (strong, nonatomic) NSString *defaultThumbnailURL;
@property (strong, nonatomic) NSString *highThumbnailURL;
@property (strong, nonatomic) NSString *mediumThumbnailURL;


//Create the YouTubeVideo Object from the Fetch Response Dictionary
+ (YouTubeVideo*) YouTubeVideoWithDictionary: (NSDictionary*) dict;

@end
