//
//  ViewController.h
//  SurflineVideos
//
//  Created by Javier de Castro on 12/12/15.
//  Copyright © 2015 Javidecas. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController <UITextFieldDelegate>



@property (weak, nonatomic) IBOutlet UITextField *searchTermTextField;
@property (weak, nonatomic) IBOutlet UIView *videoView;
@property (weak, nonatomic) IBOutlet UILabel *searchLabel;
@property (weak, nonatomic) IBOutlet UIButton *searchButton;
@end

