//
//  YouTubeVideosCollectionViewController.h
//  SurflineVideos
//
//  Created by Javier de Castro on 12/12/15.
//  Copyright © 2015 Javidecas. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YouTubeVideosCollectionViewController : UICollectionViewController


//Array with results from HTTP Request
@property (strong,nonatomic)NSArray* youTubeVideoResults;
@end
