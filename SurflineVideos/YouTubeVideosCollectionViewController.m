//
//  YouTubeVideosCollectionViewController.m
//  SurflineVideos
//
//  Created by Javier de Castro on 12/12/15.
//  Copyright © 2015 Javidecas. All rights reserved.
//

#import "YouTubeVideosCollectionViewController.h"
#import "YouTubeVideo.h"
#import "YouTubeVideoCollectionViewCell.h"
#import "YouTubePlayerViewController.h"
@interface YouTubeVideosCollectionViewController ()

//Mutable Array where the thumbnail images will be stored
@property (strong,nonatomic)NSMutableArray* preloadedThumbnails;

@end

@implementation YouTubeVideosCollectionViewController
{
    //Video Selected to be passed to details ViewController
    YouTubeVideo * selectedVideo;
    
}
static NSString * const reuseIdentifier = @"youTubeVideoCell";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //Unhide Navbar
    [self.navigationController.navigationBar setHidden:NO];
    
    // Register custom Cell Class
    UINib *cellNib = [UINib nibWithNibName:@"YouTubeVideoCollectionViewCell" bundle:nil];
    [self.collectionView registerNib:cellNib forCellWithReuseIdentifier:reuseIdentifier];
    
    //Initially the thumbnails are not downloaded so we prepopulate them with a preset image from assets
    [self initEmptyThumbnails];
    //Start process of downloading the thumbnails and updating them
    [self preloadThumbNails];
    
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"YOUTUBE_VIDEO_PLAYER_SEGUE"]) {
        
        //Pass the selected video to the details ViewController
        YouTubePlayerViewController * dest=segue.destinationViewController;
        dest.video=selectedVideo;
    }
    
}


#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    //Only 1 section
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    //As Mant rows as videos in the results Array
    return [self.youTubeVideoResults count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    //Dequeue Cell
    YouTubeVideoCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    //Get Video at cell's index
    YouTubeVideo * video=[self.youTubeVideoResults objectAtIndex:indexPath.row];
    //Get Videos Thumbnail ( Same index as cell and as Video  )
    UIImageView *imageView = [self.preloadedThumbnails objectAtIndex:indexPath.row];
    // Configure the cell
    //Set Image
    cell.thumbNailImage.image=imageView.image;
    //Set title
    [cell.titleLabel setText:video.title];
    //Round Cell Borders
    cell.layer.masksToBounds = YES;
    cell.layer.cornerRadius = 6;
    //Round Titles Background View Borders too
    cell.titleBackgroundView.layer.masksToBounds=YES;
    cell.titleBackgroundView.layer.cornerRadius = 6;
    
    return cell;
}



#pragma mark <UICollectionViewDelegate>


-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    //Update Selected Video Variable
    selectedVideo=[self.youTubeVideoResults objectAtIndex:indexPath.row];
    // Go to Video Details (performSegueWithIdentifier must be done in main Thread )
    dispatch_async(dispatch_get_main_queue(), ^(){
        [self performSegueWithIdentifier:@"YOUTUBE_VIDEO_PLAYER_SEGUE" sender:self];
    });
    
}

#pragma mark Helper Functions

//Initially the thumbnails are not downloaded so we prepopulates them with a preset image from assets

-(void)initEmptyThumbnails
{
    //Init Array
    self.preloadedThumbnails=[[NSMutableArray alloc]init];
    //PrePopulate Array with image from assets
    for (int index=0; index<self.youTubeVideoResults.count; index++) {
        UIImage *img = [UIImage imageNamed:@"thumbnail.jpeg"];
        UIImageView *imageView = [[UIImageView alloc] initWithImage:img];
        [self.preloadedThumbnails addObject:imageView];
        
    }
    
}

//Process of downloading the thumbnails and updating them
-(void)preloadThumbNails
{
    
    //Perform this process in a background thread ( Avoid Blocking the UI )
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        for (int index=0; index<self.youTubeVideoResults.count; index++) {
            //Get thumbnails URL From Video Object and Download it
            YouTubeVideo * video = [self.youTubeVideoResults objectAtIndex:index];
            NSURL *url = [NSURL URLWithString:video.defaultThumbnailURL];
            NSData *data = [NSData dataWithContentsOfURL:url];
            UIImage *image = [UIImage imageWithData:data];
            UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
            //Replace Prepopulated image with real thumbnail
            [self.preloadedThumbnails replaceObjectAtIndex:index withObject:imageView];
            
            //Tell CollectionView to refresh and show new thumbnail, this must be done in the Main Thread
            dispatch_async(dispatch_get_main_queue(), ^(){
                [self.collectionView reloadData];
            });
        }
    });
}


@end
