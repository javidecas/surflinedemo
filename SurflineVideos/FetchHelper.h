//
//  FetchHelper.h
//  SurflineVideos
//
//  Created by Javier de Castro on 12/12/15.
//  Copyright © 2015 Javidecas. All rights reserved.
//

#import <Foundation/Foundation.h>


//define blocks

//Define ErrorBlock with an NSError as argument
typedef void (^ErrorBlock)(NSError* error);
//Define ArrayResponseBlock with a NSMutableArray as argument
typedef void (^ArrayResponseBlock)(NSMutableArray *response);


//Google API  ( Since this app does not use any user related info no Oauth is needed
# define KEY @"AIzaSyDeRK8DN9k_0zQwJa18nQJQmAtE36mbPGc"

//This Demo App will use only the Search: list API, the base URL is the following
#define SEARCH_API_BASE_URL_STRING @"https://www.googleapis.com/youtube/v3/search"




//This class is the Core of this small project, it is the Class in charge of interacting with the YouTube API And retrieving the information
@interface FetchHelper : NSObject


//Class Function to search Videos Related with searchTerm keyword
+ (void) searchVideosAbout:(NSString *) searchTerm
             onCompletion: (ArrayResponseBlock) completion
                  onError: (ErrorBlock) errorBlock;

@end
