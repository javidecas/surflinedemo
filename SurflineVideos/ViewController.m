//
//  ViewController.m
//  SurflineVideos
//
//  Created by Javier de Castro on 12/12/15.
//  Copyright © 2015 Javidecas. All rights reserved.
//

#import "ViewController.h"
#import "FetchHelper.h"
#import "YouTubeVideosCollectionViewController.h"
#import "YouTubeVideo.h"
#import "MBProgressHud.h"
#import <AVFoundation/AVFoundation.h>

@interface ViewController ()
{
    MBProgressHUD * hud;
    AVPlayer *player ;
}
@end

@implementation ViewController
{
    NSArray * videoResults;
    NSMutableArray * preloadedThumbnails;
    
}

#pragma mark - App LifeCycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //Setup Action Bar Colors
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    
    //Customize Search Button
    [[self.searchButton layer] setBorderWidth:1.0f];
    [[self.searchButton layer] setBorderColor:[UIColor whiteColor].CGColor];
    [self.searchButton.layer setCornerRadius:8];
    
    //Handle dismiss keyboard when tapping the screen
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTapViewOnce:)];
    [tapGesture setNumberOfTapsRequired:1];
    [tapGesture setNumberOfTouchesRequired:1];
    [self.view addGestureRecognizer:tapGesture];
    
    //Set the TextField delegate
    [self.searchTermTextField setDelegate:self];
    
    //Init the video Background
    [self initVideoPlayer];
    
    
    
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.navigationController.navigationBar setHidden:YES];
    
    //Move Label and TextField so we can animate them in
    [self.searchLabel setCenter:CGPointMake(self.searchLabel.center.x - self.view.bounds.size.width, self.searchLabel.center.y)];
    
    [self.searchTermTextField setCenter:CGPointMake(self.searchTermTextField.center.x - self.view.bounds.size.width, self.searchTermTextField.center.y)];
    
    [self.searchButton setCenter:CGPointMake(self.searchButton.center.x - self.view.bounds.size.width, self.searchButton.center.y)];
    
    
    // Add Observer so we can loop the video
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playerItemDidReachEnd:)
                                                 name:AVPlayerItemDidPlayToEndTimeNotification
                                               object:[player currentItem]];
    
    // Add Observer so we stop the video when moving app to background
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationDidEnterBackground:) name:UIApplicationDidEnterBackgroundNotification object:nil];
    
    //Play Video
    [player play];
    
}


-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    //Pause PLayer and set it to start from the beginning whenever we play it again
    [player seekToTime:kCMTimeZero];
    [player pause];
    
    //Remove observer for all notifications
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
}



- (void)applicationDidEnterBackground:(NSNotification *)notification
{
    //Pause PLayer and set it to start from the beginning whenever we play it again
    [player seekToTime:kCMTimeZero];
    [player pause];
    //Remove observer for all notifications
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    //Add observer to detect App back in Foreground
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationiWllEnterForeground:) name:UIApplicationWillEnterForegroundNotification object:nil];
    
    
}
- (void)applicationiWllEnterForeground:(NSNotification *)notification
{
    //Remove Observers
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    //Re-Add video looping observer
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playerItemDidReachEnd:)
                                                 name:AVPlayerItemDidPlayToEndTimeNotification
                                               object:[player currentItem]];
    
    
    
    //Restart Video
    [player play];
    
    //Re-Add Observer so we stop the video when moving app to background
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationDidEnterBackground:) name:UIApplicationDidEnterBackgroundNotification object:nil];
    
}




-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    //Animate in the Label ,TextField and Button
    [self animateInViews];
}


#pragma mark - Animation and VideoPlayer Helpers


-(void)animateInViews
{
    
    //Animate Label and TextField
    [UIView animateWithDuration:1 delay:0 options:UIViewAnimationOptionCurveEaseInOut  animations:^{
        
        
        [self.searchLabel setCenter:CGPointMake(self.searchLabel.center.x + self.view.bounds.size.width, self.searchLabel.center.y)];
        
        [self.searchTermTextField setCenter:CGPointMake(self.searchTermTextField.center.x + self.view.bounds.size.width, self.searchTermTextField.center.y)];
        
    } completion:nil];
    
    
    //Animate Button after 1 second
    
    [UIView animateWithDuration:1 delay:1 options:UIViewAnimationOptionCurveEaseInOut  animations:^{
        
        [self.searchButton setCenter:CGPointMake(self.searchButton.center.x + self.view.bounds.size.width, self.searchButton.center.y)];
        
    } completion:nil];
    
}



//Init Video Player
-(void)initVideoPlayer
{
    NSBundle *bundle = [NSBundle mainBundle];
    NSString *moviePath = [bundle pathForResource:@"background" ofType:@"mp4"];
    NSURL *movieURL = [NSURL fileURLWithPath:moviePath];
    player = [AVPlayer playerWithURL:movieURL];
    
    AVPlayerLayer *layer = [AVPlayerLayer layer];
    [player setVolume:0.0];
    [layer setPlayer:player];
    [layer setFrame:self.videoView.frame];
    [layer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
    [self.videoView.layer addSublayer:layer];
    
}


//Restart Player
- (void)playerItemDidReachEnd:(NSNotification *)notification {
    [player seekToTime:kCMTimeZero];
    [player play];
}


#pragma mark - IBActions

- (IBAction)didTapSearch:(id)sender {
    
    
    //Show Loader while performing the HTTP Request
    [self showHUDWithName:@"Loading Results..."];
    
    
    //Use The Helper Class we created to search videos related with the keyword entered in the TextField
    [FetchHelper searchVideosAbout:self.searchTermTextField.text onCompletion:^(NSMutableArray *response) {
        
        //If success perform the onCompletion Block
        //Update videoResults variable with the fetch result
        videoResults=response;
        
        //Hide Loader and go to YouTubeVideosCollectionViewController to show results
        //Those operations must be done in the main thread:
        dispatch_async(dispatch_get_main_queue(), ^{
            if (hud)
            {
                [hud hide:YES];
            }
            [self performSegueWithIdentifier:@"SEARCH_RESULTS_SEGUE" sender:self];
        });
    } onError:^(NSError *error) {
        
        //In Case of error reported, show alert View with the error message
        [self showAlertviewWithTitle:@"Error" andMessage:error.localizedDescription];
    }];
}


#pragma mark - AlertView and Loader View Helpers


- (void)showHUDWithName:(NSString *)name{
    
    //Make sure that all of those UI Operations are performed on the Main Thread
    dispatch_async (dispatch_get_main_queue(), ^{
        if (hud)
        {
            [hud hide:YES];
        }
        hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        hud.mode = MBProgressHUDModeIndeterminate;
        hud.labelText = name;
        
    });
}

- (void)showAlertviewWithTitle:(NSString *)title andMessage:(NSString *)msg
{
    //Make sure that all of those UI Operations are performed on the Main Thread
    dispatch_async(dispatch_get_main_queue(), ^{
        if (hud)
        {
            [hud hide:YES];
        }
        UIAlertController* alertController = [UIAlertController alertControllerWithTitle:title message:msg preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
        [alertController addAction:okAction];
        [self.navigationController presentViewController:alertController animated:YES completion:nil];
    });
}




#pragma mark - TextField Delegate

-(BOOL) textFieldShouldReturn:(UITextField *)textField
{
    [self didTapSearch:nil];
    return YES;
}


- (IBAction)didTapViewOnce:(UITapGestureRecognizer *)sender
{
    [self dismissKeyboard];
}


- (void)dismissKeyboard
{
    [self.searchTermTextField resignFirstResponder];
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"SEARCH_RESULTS_SEGUE"]) {
        YouTubeVideosCollectionViewController * dest=segue.destinationViewController;
        
        //Pass the fetch results to the CollectionViewController to be presented
        dest.youTubeVideoResults=videoResults;
    }
}

@end
