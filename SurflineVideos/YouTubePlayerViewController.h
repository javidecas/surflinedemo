//
//  YouTubePlayerViewController.h
//  SurflineVideos
//
//  Created by Javier de Castro on 12/13/15.
//  Copyright © 2015 Javidecas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YTPlayerView.h"
#import "YouTubeVideo.h"
@interface YouTubePlayerViewController : UIViewController



//YouTube Player, Used Cocoapods to integrate it with the project
@property(nonatomic, strong) IBOutlet YTPlayerView *playerView;


//Video Selected
@property(nonatomic, strong)  YouTubeVideo *video;

//Short Description
@property (weak, nonatomic) IBOutlet UITextView *videoDescriptionTextView;
//Date
@property (weak, nonatomic) IBOutlet UILabel *uploadDate;
//Background Views
@property (weak, nonatomic) IBOutlet UIView *descriptionView;
@property (weak, nonatomic) IBOutlet UIView *dateView;
@end
