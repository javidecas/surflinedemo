//
//  YouTubePlayerViewController.m
//  SurflineVideos
//
//  Created by Javier de Castro on 12/13/15.
//  Copyright © 2015 Javidecas. All rights reserved.
//

#import "YouTubePlayerViewController.h"
#import "YouTubeVideo.h"
@interface YouTubePlayerViewController ()

@end

@implementation YouTubePlayerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //From the YouTube Documentation:
    // set playerVars to playsinline so Video is not played in Full Screen Mode Unless Full Screen Button Pressed
    NSDictionary *playerVars = @{
                                 @"playsinline" : @1,
                                 };
    
    //From YouTube Doc, init the player passing the video's ID
    [self.playerView loadWithVideoId:self.video.videoID playerVars:playerVars];
    
    //Set Short Description Text
    [self.videoDescriptionTextView setText:self.video.desc];
    
    //Set Date Text ( Formatted in Human-Readable format )
    [self.uploadDate setText:[self formatDate:self.video.publishedAt]];
    
    //Round Background Views
    self.descriptionView.layer.masksToBounds = YES;
    self.descriptionView.layer.cornerRadius = 6;
    
    self.dateView.layer.masksToBounds = YES;
    self.dateView.layer.cornerRadius = 6;
}



//Helper function to Format Date
- (NSString *)formatDate:(NSString *)unformatedDate
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSZ"];
    NSDate* myDate = [dateFormatter dateFromString:unformatedDate];
    
    [dateFormatter setDateFormat:@"MMM dd, yyyy hh:mm a"];
    NSString *stringFromDate = [dateFormatter stringFromDate:myDate];
    
    return stringFromDate;
}

@end
