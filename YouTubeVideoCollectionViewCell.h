//
//  YouTubeVideoCollectionViewCell.h
//  SurflineVideos
//
//  Created by Javier de Castro on 12/12/15.
//  Copyright © 2015 Javidecas. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YouTubeVideoCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *thumbNailImage;
@property (weak, nonatomic) IBOutlet UIView *titleBackgroundView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@end
