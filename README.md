This Sample Project Uses the Public YouTube Data API to search for the first 25 videos matching a given keyword. Once selected a video, the App allows the user to see some details about it ( Like a short description and the Date of Upload )  and  to reproduce it on an embedded youtube player.

Information about the API can be found at:

https://developers.google.com/youtube/v3/docs/search/list


All the Network operations are abstracted in the FetchHelper Class, If different queries were to be added to this project this would be the class where the API interaction implementation must be coded.

I approached the solution by using blocks instead of protocols making the code easier to read and maintain.


I defined two blocks : 
 - ^ErrorBlock  that accepts a NSError as argument

 - ^ArrayResoponseBlock that accepts a NSMutableArray as an argument ( the search operation calls  this block with an array of YouTubeVideo Objects)



The Class YouTubeVideo models an Object with the most relevant information related with the videos fetched.


To demonstrate the ability to interact with 3rd party frameworks, this project uses CocoaPods ( Therefore in order to open the project in Xcode the SurflineVideos.xcworkspace file must be selected nor the SurflineVideos.xcodeproj)

Pods Used:

- pod "youtube-ios-player-helper", "~> 0.1.1"

Used for the Embedded Video Player

pod 'MBProgressHUD', '~> 0.9.1'

Used for the Loader shown while fetching videos




Contact Info:
* Javier de Castro : javidecas@gmail.com